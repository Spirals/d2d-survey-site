from setuptools import find_packages, setup

setup(
    name='d2d_survey',
    version='0.1',
    author='Adrien Luxey',
    author_email='adrien.luxey@inria.fr',
    description='A survey site on D2D usage',
    url='https://gitlab.inria.fr/Spirals/d2d-survey-site',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask >= 2',
        'flask-babel >= 2',
    ],
)