import os, json, hashlib
from datetime import datetime
from secrets import token_hex
from flask import request, render_template, current_app, flash, redirect, url_for, g
from flask_babel import gettext, lazy_gettext

GUID_NBYTES=16

def handle_form():
    if request.method == 'GET':
        return render_template(f'static/form.html', guid=token_hex(GUID_NBYTES))
    elif request.method == 'POST':
        # Captcha: avoid stupid bots
        if not "15" in request.form['captcha'] and \
                not "fifteen" in request.form['captcha'].lower() and \
                not "quinze" in request.form['captcha'].lower():
            # Inform the user that they are a robot
            to_flash = lazy_gettext("From our point of view, you are a robot: you failed the anti-robot question.")
            flash(to_flash, 'error')
            # Re-render form, providing the previous form answers
            return render_template(f'static/form.html', 
                prev_ans=dict(request.form),
                guid=request.form['guid'])
        # GUID: prevent double sends
        if guid_exists(request.form['guid']):
            return redirect(url_for('frontend.static', page='thank_you'))

        try:
            save_results()
        except Exception as e:
            flashed = lazy_gettext("An error occured while saving your results: ")
            flashed += str(e)
            flash(flashed, 'error')
            return render_template(f'static/form.html', guid=request.form['guid'])
        else:
            return redirect(url_for('frontend.static', page='thank_you'))


def guid_exists(guid):
    return os.path.exists(os.path.join(
        current_app.config['OUTPUT_DIR'], f"{guid}.json"))

def get_remote_ip():
    if request.headers.getlist("X-Forwarded-For"):
       return request.headers.getlist("X-Forwarded-For")[0]
    else:
       return request.remote_addr

def save_results():
    form = request.form.copy()
    fn = os.path.join(
        current_app.config['OUTPUT_DIR'], f"{form['guid']}.json")
    sha1 = hashlib.sha1()
    sha1.update(get_remote_ip().encode('utf-8'))
    sha1.update(request.user_agent.string.encode('utf-8'))

    with open(fn, 'w') as f:
        del form['guid']
        del form['captcha']
        form['date'] = datetime.utcnow().isoformat()
        form['user_hash'] = sha1.hexdigest()
        json.dump(form, f)