from flask import render_template

def page_not_found(e):
    return render_template('errors/404.html'), 404

def not_allowed(e):
    return render_template('errors/403.html'), 403

def internal_server_error(e):
    return render_template('errors/500.html'), 500
