import os 

from flask import Flask, redirect, url_for
from flask.cli import AppGroup
from flask_babel import Babel

babel = Babel()
translate_cli = AppGroup('translate')

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.environ['FLASK_SECRET_KEY'],
        OUTPUT_DIR=os.environ.get('FLASK_OUTPUT_DIR', 
            os.path.join(os.getcwd(), 'output')),
        BABEL_TRANSLATION_DIRECTORIES=\
            os.path.join(os.getcwd(), 'translations'),
        BABEL_DEFAULT_LOCALE='fr',
        LANGUAGES = {
            'fr': 'Français',
            'en': 'English'
        }
    )

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    babel.init_app(app)

    from . import cli, locale, survey
    app.cli.add_command(translate_cli)

    # Ensure output directory exists
    try:
        os.makedirs(app.config['OUTPUT_DIR'])
    except OSError:
        pass

    from . import errors, blueprint
    app.register_error_handler(404, errors.page_not_found)
    app.register_error_handler(403, errors.not_allowed)
    app.register_error_handler(500, errors.internal_server_error)
    app.register_blueprint(blueprint.bp)

    @app.route('/')
    def index():
        return redirect(url_for( 'frontend.static', page='index' ))

    return app